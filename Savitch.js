var low = require('lowdb');
var fs = require('fs-extra');
var md5 = require('crypto-js/md5');

var Savitch = function(db) {
	////////////////////////////////////////////////// Databases get from parameters
	this.db = low('./db/' + db.main);
	this.contactdb = low('./db/' + db.contact);

	if (db.dashboard) {
		this.dashboard = low('./db/' + db.dashboard);
	}
	//////////////////////////////////////////////////\

	this.getConfig = function(which) { // Return configuration. 'which': undefined, "dashboard" (default: undefined).
		if (which == 'dashboard') {
			return this.dashboard('config').value();
		} else {
			return this.db('config').value();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.getAside = function(which, status) { // Returns aside. 'which'== undefined, "dashboard" (default== undefined). 'status'== undefined, "all" (default== undefined)	
		if (which == 'dashboard') {
			if (status == 'all') {
				return this.dashboard('aside').chain().value();
			} else {
				return this.dashboard('aside').chain().where({active: true}).value();
			}
		} else {
			if (status == 'all') {
				return this.db('aside').chain().value();
			} else {
				return this.db('aside').chain().where({active: true}).value();
			}
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////// articles /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.getArticles = function(status) { // Return all articles. 'status'== undefined, "offline", "all" (default== undefined)
		if(status == 'all') {
			return this.db('articles').value();
		} else if(status == 'offline') {
			return this.db('articles').chain().where({"online": false}).value();
		} else {
			return this.db('articles').chain().where({"online": true}).value();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.countArticles = function(status) { // Get the number of articles. 'status'== undefined, "offline", "all" (default== undefined)
		if(status == 'all') {
			return this.db('articles').size();
		} else if (status == 'offline') {
			return this.db('articles').chain().where({"online": false}).size();
		} else {
			return this.db('articles').chain().where({"online": true}).size();
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////   shop   /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.getShop = function(status) { // Return all merchs. 'status'== undefined, "offline", "all" (default== undefined)
		if(status == 'all') {
			return this.db('shop').value();
		} else if (status == 'offline') {
			return this.db('shop').chain().where({"online": false}).value();
		} else {
			return this.db('shop').chain().where({"online": true}).value();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.countShop = function(status) { // Get the number of merchs. 'status'== undefined, "offline", "all" (default== undefined)
		if(status == 'all') {
			return this.db('shop').size();
		} else if (status == 'offline') {
			return this.db('shop').chain().where({"online": false}).size();
		} else {
			return this.db('shop').chain().where({"online": true}).size();
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////// messages /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.getMessages = function(status) { // Return all merchs. 'status'== undefined, "unread", "all" (default== undefined)
		if(status == 'all') {
			return this.contactdb('contact').value();
		} else if (status == 'unread') {
			return this.contactdb('contact').chain().where({"read": false}).value();
		} else {
			return this.contactdb('contact').chain().where({"read": true}).value();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.countMessages = function(status) { // Get the number of merchs. 'status'== undefined, "unread", "read" (default== undefined)
		if(status == 'all') {
			return this.contactdb('contact').value().length;
		} else if (status == 'unread') {
			return this.contactdb('contact').chain().where({"read": false}).value().length;
		} else {
			return this.contactdb('contact').chain().where({"read": true}).value().length;
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.getOneMessage = function(location) { // Return a specific message. !'location'== md5()!
		return this.contactdb('contact').chain().where({"location": location}).value()[0];
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.putunreadMessages = function(location) { // Put one specific message "read" = false. !'location'== md5()!
		console.log('unread');
		this.contactdb('contact').chain().find({"location": location}).assign({"read": false}).value();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.putreadMessages = function(location) { // Put one specific message "read" = true. !'location'== md5()!
		console.log('read');
		this.contactdb('contact').chain().find({"location": location}).assign({"read": true}).value();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.deleteMessages = function(location) { // Get the number of merchs. !'location'== md5()!
		this.contactdb('contact').remove({"location": location});
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////// formating /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.getFormatedDate = function() { // Return a formatted date
		return new Date().getDate() + '/' + new Date().getMonth() + '/' + new Date().getFullYear();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.getFormatedDescription = function(text, jumper) { //Return an array with formatted description. 'text' is the string to parse and 'jumper' is the character which is detected.

		var count = 0;
		var description = [''];

		for (var i = 0; i < text.length; i++) {
			if(text[i] == jumper[0]) {
				description.push('');
				count++;
			} else {
				description[count] += text[i];
			}
		}

		return description;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////  Patterns for main :  /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.byPattern = function(pattern, data) { //The website works on pattern in JSON objects returned to the views (main)

		if(pattern == 'index' || pattern == '/') {	//For index page (main)
			var completedObject = {
				title: this.getConfig().defaultTitle,
				blogOwner: this.getConfig().blogOwner,
				aside: this.getAside(),
				articles: this.getArticles()
			}
		}

		if(pattern == 'contact' || pattern == '/contact') {	//For contact page (main)
			var completedObject = {
				title: this.getConfig().defaultTitle,
				blogOwner: this.getConfig().blogOwner,
				aside: this.getAside(),
			}
		}

		if(pattern == 'article' || pattern == '/article' || pattern == '/lightbox/article') {	//For article page (main)
			var completedObject = {
				article: this.db('articles').chain().where({location: data}).value()[0]
			}
		}

		if(pattern == 'shop' || pattern == '/shop') {	//For shop page (main)
			var completedObject = {
				title: this.getConfig().defaultTitle,
				blogOwner: this.getConfig().blogOwner,
				aside: this.getAside(),
				articles: this.db('shop').chain().value()
			}
		}

		if(pattern == 'buy' || pattern == '/buy') {	//For buy page (main)
			var completedObject = {
				article: this.db('shop').chain().where({id: data}).value()[0]
			}
		}

		return completedObject;
	};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////  Patterns for dashboard :  ///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.byPatternDashboard = function(pattern, data) { //The website works on pattern in JSON objects returned to the views (dashboard)
		
		if(pattern == 'index' || pattern == '/dashboard/') { //For index page (dasboard)
			var completedObject = {
				title: this.getConfig('dashboard').defaultTitle,
				blogOwner: this.getConfig('dashboard').blogOwner,
				aside: this.dashboard('aside').chain().where({active: true}).value(),
				informations: {
					articles: {
						online: this.countArticles(),
						offline: this.countArticles('offline')
					},
					shop: {
						online: this.countShop(),
						offline: this.countShop('shop')
					}
				}
			}
		}

		if(pattern == 'articles' || pattern == '/dashboard/articles') { //For articles management page (dashboard)
			var completedObject = {
				title: this.getConfig('dashboard').defaultTitle,
				blogOwner: this.getConfig('dashboard').blogOwner,
				aside: this.getAside('dashboard'),
				onlines: this.getArticles(),
				offlines: this.getArticles('offline')
			}
		}

		if(pattern == 'shop' || pattern == '/dashboard/shop') { //For shop management page (dashboard)
			var completedObject = {
				title: this.getConfig('dashboard').defaultTitle,
				blogOwner: this.getConfig('dashboard').blogOwner,
				aside: this.getAside('dashboard'),
				onlines: this.getShop(),
				offlines: this.getShop('offline')
			}
		}

		if(pattern == 'messages' || pattern == '/dashboard/messages') { //For messages management page (dashboard)
			var completedObject = {
				title: this.getConfig('dashboard').defaultTitle,
				blogOwner: this.getConfig('dashboard').blogOwner,
				aside: this.getAside('dashboard'),
				read: this.getMessages(),
				unread: this.getMessages('unread')
			}
		}

		if(pattern == 'readMessage' || pattern == '/read/messages') { //For messages management page (dashboard)
			var completedObject = {
				title: this.getConfig('dashboard').defaultTitle,
				blogOwner: this.getConfig('dashboard').blogOwner,
				aside: this.getAside('dashboard'),
				message: this.getOneMessage(data)
			}
		}
	
		return completedObject;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////// database push ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.pushContact = function(info) { //Function that push a contact ask into database.
		info.date = this.getFormatedDate();

		this.contactdb('contact').push({
			location: md5(Math.random() + info.name) + '',
			name: info.name,
			email: info.email,
			subject: info.subject,
			content: info.content,
			date: info.date,
			read: false
		});
	}

	this.pushArticle = function(info) { //Function that verify and push an article in database.

		var title = info.body.title;
		var location = info.body.location;
		var filename = info.body.filename;
		var alt = info.body.alt;

		if(!info.body.date) {
			var date = this.getFormatedDate();
		} else {
			var date = info.body.date;
		}

		if(info.body.description) {
			var description = this.getFormatedDescription(info.body.description, '下');
			console.log(description);
		} else {
			var description = [];
		}

		this.db('articles').push({
			title: title,
			date: date,
			online: true,
			location: location,
			cover: {
				filename: filename,
				alt: alt
			},
			description: description,
			pictures: [],
			stats: {
				views: 0
			}
		});
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////// session management ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this.verifyPassword = function(SESSION, functions, password) { //SESSION: Object(), functions: {"connection": function(), "page": function()}, password: md5("text").
		if (!SESSION.password) {
			return {
				"execute": functions.connection,
				"SESSION": SESSION
			};
		} else if (SESSION.password == password) {
			SESSION.cookie.expires = 3600 * 1000;
			SESSION.password = password;
			return {
				"execute": functions.page,
				"SESSION": SESSION
			};
		} else {
			return {
				"execute": functions.connection,
				"SESSION": SESSION
			};
		}
	}

} 

module.exports = Savitch;