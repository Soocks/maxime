var express = require('express');
var router = express.Router();
var Savitch = require('../Savitch');
var fs = require('fs-extra');
Savitch = new Savitch({main:'main.json', contact: 'contact.json', dashboard: 'dashboard.json'});
var md5 = require('crypto-js/md5');

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', Savitch.byPattern('index'));
});

router.get('/contact', function(req, res, next) {
	res.render('contact', Savitch.byPattern('contact'));
});

router.get('/lightbox/:article', function(req, res, next) {
	var article = req.params.article;
	res.render('lightbox', Savitch.byPattern('article', article));
});

router.get('/shop', function(req, res, next) {
	res.render('shop', Savitch.byPattern('shop'));
});

router.get('/shop/:itemNum', function(req, res, next) {
	var itemNum = parseInt(req.params.itemNum);
	res.render('buy', Savitch.byPattern('buy', itemNum));
});

router.post('/contact', function(req, res, next) {
	Savitch.pushContact(req.body);
	res.render('successfully_sent', Savitch.byPattern('contact'));
});

////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////Dashboard////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

var password = '054ba43bb613d8686a4063900b492e63';

router.get('/dashboard/connect', function(req, res, next) {
	SESSION = req.session;
	res.render('dashboard/connect');
});

router.post('/dashboard/connect', function(req, res, next) {
	SESSION = req.session;
	if(md5(req.body.password) == password) {
		SESSION.cookie.expires = 3600 * 1000;
		SESSION.password = password;
		res.render('dashboard/index', Savitch.byPatternDashboard('index'));
	} else {
		res.render('dashboard/connect');
	}
});

router.get('/dashboard', function(req, res, next) {
	SESSION = req.session;

	if (!SESSION.password) {
		res.render('dashboard/connect');
	} else if (SESSION.password == password) {
		SESSION.cookie.expires = 3600 * 1000;
		SESSION.password = password;
		res.render('dashboard/index', Savitch.byPatternDashboard('index'));
	} else {
		res.render('dashboard/connect');
	}
});

router.get('/dashboard/articles', function(req, res, next) {
	SESSION = req.session;

	if (!SESSION.password) {
		res.render('dashboard/connect');
	} else if (SESSION.password == password) {
		SESSION.cookie.expires = 3600 * 1000;
		SESSION.password = password;
		res.render('dashboard/articles', Savitch.byPatternDashboard('articles'));
	} else {
		res.render('dashboard/connect');
	}
});

router.get('/dashboard/shop', function(req, res, next) {
	SESSION = req.session;

	if (!SESSION.password) {
		res.render('dashboard/connect');
	} else if (SESSION.password == password) {
		SESSION.cookie.expires = 3600 * 1000;
		SESSION.password = password;
		res.render('dashboard/shop', Savitch.byPatternDashboard('shop'));
	} else {
		res.render('dashboard/connect');
	}
});

router.get('/dashboard/create/articles', function(req, res, next) {
	SESSION = req.session;

	if (!SESSION.password) {
		res.render('dashboard/connect');
	} else if (SESSION.password == password) {
		SESSION.cookie.expires = 3600 * 1000;
		SESSION.password = password;
		res.render('dashboard/createArticle', Savitch.byPatternDashboard('index'));
	} else {
		res.render('dashboard/connect');
	}
});

router.post('/dashboard/make_new/article', function(req, res, next) {
	SESSION = req.session;

	if (!SESSION.password) {
		res.render('dashboard/connect');
	} else if (SESSION.password == password) {
		SESSION.cookie.expires = 3600 * 1000;
		SESSION.password = password;
		Savitch.pushArticle({body: req.body});
	} else {
		res.render('dashboard/connect');
	}
});

router.get('/dashboard/messages', function(req, res, next) {
	SESSION = req.session;

	if (!SESSION.password) {
		res.render('dashboard/connect');
	} else if (SESSION.password == password) {
		SESSION.cookie.expires = 3600 * 1000;
		SESSION.password = password;
		res.render('dashboard/messages', Savitch.byPatternDashboard('messages'));
	} else {
		res.render('dashboard/connect');
	}
});

router.get('/dashboard/read/messages/:message', function(req, res, next) {
	SESSION = req.session;

	if (!SESSION.password) {
		res.render('dashboard/connect');
	} else if (SESSION.password == password) {
		SESSION.cookie.expires = 3600 * 1000;
		SESSION.password = password;
		var message = req.params.message;
		Savitch.putreadMessages(message);
		res.render('dashboard/readMessage', Savitch.byPatternDashboard('/read/messages', message));
	} else {
		res.render('dashboard/connect');
	}
});

router.get('/dashboard/delete/messages/:message', function(req, res, next) {
	SESSION = req.session;

	if (!SESSION.password) {
		res.render('dashboard/connect');
	} else if (SESSION.password == password) {
		SESSION.cookie.expires = 3600 * 1000;
		SESSION.password = password;
		var message = req.params.message;
		Savitch.deleteMessages(message);
		res.render('dashboard/messages', Savitch.byPatternDashboard('messages'));
	} else {
		res.render('dashboard/connect');
	}
});

router.get('/dashboard/putread/messages/:message', function(req, res, next) {
	SESSION = req.session;

	if (!SESSION.password) {
		res.render('dashboard/connect');
	} else if (SESSION.password == password) {
		SESSION.cookie.expires = 3600 * 1000;
		SESSION.password = password;
		var message = req.params.message;
		Savitch.putreadMessages(message);
		res.render('dashboard/messages', Savitch.byPatternDashboard('messages'));
	} else {
		res.render('dashboard/connect');
	}
});

router.get('/dashboard/putunread/messages/:message', function(req, res, next) {
	SESSION = req.session;

	if (!SESSION.password) {
		res.render('dashboard/connect');
	} else if (SESSION.password == password) {
		SESSION.cookie.expires = 3600 * 1000;
		SESSION.password = password;
		var message = req.params.message;
		Savitch.putunreadMessages(message);
		res.render('dashboard/messages', Savitch.byPatternDashboard('messages'));
	} else {
		res.render('dashboard/connect');
	}
});

module.exports = router;